import { Component, OnInit } from '@angular/core';
declare var videojs: any;
declare var pbjs: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  private player: any;
  ngOnInit() {
    const prebIdVideo = localStorage.getItem('videoUrl');
    this.invokeVideoPlayer(prebIdVideo);
  }
  constructor() { }
  invokeVideoPlayer(url: any) {
    let player = videojs('vid1');
    player.ready(() => {
      player.vastClient({
        adTagUrl: url,
        playAdAlways: true,
        verbosity: 0,
        vpaidFlashLoaderPath: "https://github.com/MailOnline/videojs-vast-vpaid/blob/RELEASE/bin/VPAIDFlash.swf?raw=true",
        autoplay: true
      });
      player.muted(true);
      player.play();
    });
  }

}

  

